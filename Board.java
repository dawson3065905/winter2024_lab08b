import java.util.Random;

public class Board{
	private Tile[][] grid;
	
	public Board(){
		//size
		final int BOARDSIZE = 5;
		
		Random randomizer = new Random();
		
		grid = new Tile[BOARDSIZE][BOARDSIZE];
		
		//has to use c style for loops because 
		//for each loops cannot modify, only read
		for(int i = 0; i < grid.length; i++){
			//sets every tile i to blank constant from enum
			//extract index i of grid, so the first level
			for(int j = 0; j < grid[i].length; j++){
				grid[i][j] = Tile.BLANK;
			} 
		}
		
		//wall randomizer
		for(int r = 0; r < grid.length; r++){
			//random index
			int randX = randomizer.nextInt(grid[r].length);

			//no need for another for loop
			//we know exactly which index to take in each row
			grid[r][randX] = Tile.HIDDEN_WALL;
		}
		
		
	}
	
	public String toString(){
		//extra spaces in returnString for aligning column indices
		String returnString = "  ";

		//row indices, required since i am using a for each loop
		int rowIndex = 0;

		//for column index
		for(int i = 0; i < grid[0].length; i++){
			returnString += i + " ";
		}

		//line return for column indices
		returnString += "\n";

		for(Tile[] ta : grid){
			//adds index for row
			returnString += rowIndex + " ";

			for(Tile t : ta){
				//adds the value of stored tile to the output
				//incrementally builds output
				returnString += t.getName() + " ";
				
			}
			
			//new line
			//declared outside so it only adds new every
			//3 values
			returnString += "\n";
			rowIndex++;
		}
		
		return returnString;
	}
	
	public int placeToken(int row, int col){
		//checks you are actually placing the token inside the board
		if( (row >= grid.length || row < 0) || (col >= grid[0].length || col < 0) )
			return -2;
		
		//check if CASTLE or WALL
		else if(grid[row][col] == Tile.WALL || grid[row][col] == Tile.CASTLE)
			return -1;
		
		//check if HIDDEN_WALL
		else if(grid[row][col] == Tile.HIDDEN_WALL){
			grid[row][col] = Tile.WALL;
			return 1;
		}
			

		//otherwise, let player place area
		else{
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
			
	}
	
}