import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
		
		Scanner scanner = new Scanner(System.in);
		Board boarder = new Board();

		int numCastles = 5;
		int turns = 0;

		System.out.println("Hello! Welcome to Castle Placer!");

		while(numCastles > 0 && turns < 8){
			System.out.println(boarder.toString());
			System.out.println("\n Turns: " + turns + " Castles Left: " + numCastles + "\n");

			//user input
			System.out.println("Input the row: \n");
			int row = scanner.nextInt();
			System.out.println("Input the column: \n");
			int column = scanner.nextInt();

			//seperate int for storing placeToken() output
			//if i put in each of the other functions, it will work with the 
			//but it will bug out when 
			int tokenResult = boarder.placeToken(row, column);

			//checks user input
			while(tokenResult < 0){
				System.out.print("Invalid input, try again \n");

				//new user input
				System.out.println("Input the row: \n");
				row = scanner.nextInt();
				System.out.println("Input the column: \n");
				column = scanner.nextInt();

				//updates tokenResult to new value
				tokenResult = boarder.placeToken(row, column);
			}

			//while loop should catch negative exit codes
			//if there is wall
			if(tokenResult == 1){
				turns++;
				System.out.println("\n Oops, there was a wall there! \n");
			}
				
			//if it's valid
			if(tokenResult == 0){
				turns++;
				numCastles--;
				System.out.println("\n Placed Successfully! Good job! \n");
			}
				
		}

		//results
		if(numCastles == 0){
			System.out.println("You have won, congrats!");
		}
		else{
			System.out.println("Oops, you have lost, better luck next time!");
		}

		scanner.close();

	}
}